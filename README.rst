==========================================
pyArchOps - pyarchops/base docker image
==========================================


.. image:: https://img.shields.io/gitlab/pipeline/pyarchops/pyarchops/next-release.svg
        :target: https://gitlab.com/pyarchops/pyarchops/pipelines


pyarchops/base docker image suitable for tests.
This image is used by the test helpers in pyarchops/helpers.


* Free software: MIT license
* Documentation: https://pyarchops.readthedocs.io.


Features
--------

* pyarchops


Usage
--------------

.. code-block:: console

    $ docker pull image



Credits
-------

* TODO

